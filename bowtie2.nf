#!/usr/bin/env nextflow

process bowtie2_build {
// Runs bowtie2-build
//
// input:
//   path fa - Reference FASTA
//   val params - Additional Parameters
//
// output:
//   tuple => emit: idx_files
//     path(fa) - Reference FASTA
//     path("${fa}.*") - Index Files

// require:
//   params.bowtie2$dna_ref
//   params.bowtie2$bowtie2_index_parameters

  storeDir "${params.shared_dir}/${fa}/bowtie2_build"
  tag "${fa}"
  label 'bowtie2_container'
  label 'bowtie2_index'
  cache 'lenient'

  input:
  path fa
  val parstr

  output:
  tuple path(fa), path("*.bt2"), emit: idx_files

  script:
  """
  FA_BUFFER=\$(echo ${fa})
  bowtie2-build ${parstr} ${fa} \${FA_BUFFER%.fa}
  """
}


process bowtie2 {
// Runs bowtie2
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq1) - FASTQ 2
//   tuple
//     path(fa) - Reference FASTA
//     path(idx_files) - Index Files
//   parstr - Additional Parameters
//
// output:
//   tuple => emit: sams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.sam') - Output SAM File

// require:
//   FQS
//   IDX_FILES
//   params.bowtie2$bowtie2_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'bowtie2_container'
  label 'bowtie2'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq1), path(fq2)
  tuple path(fa), path(idx_files)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.sam'), emit: sams

  script:
  """
  FA_BUFFER=\$(echo ${fa})
  bowtie2 \
  -x \${FA_BUFFER%.fa} \
  -1 ${fq1} \
  -2 ${fq2} \
  --threads ${task.cpus} \
  ${parstr} \
  > ${dataset}-${pat_name}-${run}.sam
  """
}


process bowtie2_samtools_sort {
// Runs bowtie2 piped to samtools sort (to minimize storage footprint)
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq1) - FASTQ 2
//   tuple
//     path(fa) - Reference FASTA
//     path(idx_files) - Index Files
//   parstr - Additional Parameters
//
// output:
//   tuple => emit: bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path("*.bam") - Output BAM File

// require:
//   FQS
//   IDX_FILES
//   params.bowtie2$bowtie2_mem_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'bowtie2_samtools_container'
  label 'bowtie2_samtools'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq1), path(fq2)
  tuple path(fa), path(idx_files)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*sorted.bam'), emit: bams

  script:
  """
  FA_BUFFER=\$(echo ${fa})
  bowtie2 \
  -x \${FA_BUFFER%.fa} \
  -1 ${fq1} \
  -2 ${fq2} \
  --threads ${task.cpus} \
  ${parstr} \
  > tmp.bam
  samtools sort -@ ${task.cpus} tmp.bam > ${dataset}-${pat_name}-${run}.sorted.bam
  rm tmp.bam
  """
}
